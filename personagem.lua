local anim = require 'anim8'

larguraTela = love.graphics.getWidth()
alturaTela = love.graphics.getHeight()

function criaPersonagemPrincipal( )
	gravidade  = 1000
	alturaPulo = 450

	-- Definindo Personagem Principal
	personagem   = {}
	personagem.x = 100
	personagem.y = 460
	personagem.velocidadePulo = 0
	direcao      = true
	estaVivo 	 = true
	imgPersonagem = love.graphics.newImage( "Imagem/teste_robo.png")
	local p = anim.newGrid( imgPersonagem:getWidth()/8, imgPersonagem:getHeight(), imgPersonagem:getWidth(), imgPersonagem:getHeight() )
	animationPersonagem = anim.newAnimation( p( '1-8', 1 ), 0.1 )
	-- FIM: Definindo Personagem Principal

	-- Tiros Personagem Principal 
	atira 		  = true
	delayTiro 	  = 0.5
	velTiro 	  = 500
	tempoAtirar   = delayTiro
	tirosDireita  = {}
	tirosEsquerda = {}
	imgTiro 	  = love.graphics.newImage("Imagem/projetil.png")
	-- FIM:  Tiros Personagem Principal
end

function criaChefao()
	gravidadeChefao	 = 1000
	alturaPuloChefao = 350

	-- Definindo Personagem Boss
	chefao 		= {}
	chefao.x 	= 750
	chefao.y 	= 490
	chefao.velocidadePulo = 0
	chefaoVivo = true
	vidaChefao = 40
	direcaoChefao = false
	imgChefao 	= love.graphics.newImage( "Imagem/trump.png")
	local c 	= anim.newGrid( 100, 82, imgChefao:getWidth(), imgChefao:getHeight() )
	animationChefao = anim.newAnimation( c( '1-6', 1, '1-6', 2 ), 0.1 )
	-- FIM: Definindo Personagem Boss

	-- Tiros Personagem Boss
	atiraChefao 		= true
	delayTiroChefao 	= 0.2
	velTiroChefao 		= 1000
	tempoAtirarChefao 	= delayTiroChefao
	tirosDireitaChefao 	= {}
	tirosEsquerdaChefao = {}
	imgTiroChefao 		= love.graphics.newImage("Imagem/projetil2.png")
	-- FIM: Tiros Personagem Boss
end


function movimentaPersonagemPrincipal( dt )
	-- Definindo pulo
	if personagem.velocidadePulo ~= 0 then
		personagem.y = personagem.y - personagem.velocidadePulo * dt
		personagem.velocidadePulo = personagem.velocidadePulo - gravidade * dt
		if personagem.y > 460 then
			personagem.velocidadePulo = 0
			personagem.y = 460
		end
	end
	-- FIM: Definindo pulo

	-- Ações Direcionais 
	if love.keyboard.isDown( 'left' ) then
		direcao = false
		animationPersonagem:update( dt )
	end

	if love.keyboard.isDown( 'right' ) then
		direcao = true
		animationPersonagem:update( dt )
	end

	-- Pulo
	if love.keyboard.isDown( 'up' ) then
		if personagem.velocidadePulo == 0 then
			personagem.velocidadePulo = alturaPulo
		end
	end

	-- FIM: Ações Direcionais 
end

-- Função que determina a lógica para a execução dos tiros do PERSONAGEM PRINCIPAL
function atirar( dt )

	tempoAtirar = tempoAtirar - ( 1 * dt )
	if tempoAtirar < 0 then
		atira = true
	end

	-- Se a tecla pressionada for espaço e o personagem estiver habilitado a atirar, é verificado a direção para que
	-- o tiro será disparado.
	if love.keyboard.isDown( "space" ) and atira then
		if direcao then
			novoTiro = {
				x = personagem.x + 60,
				y = personagem.y + 75,
				img = imgTiro
			}
			table.insert( tirosDireita, novoTiro )
		elseif not direcao then
			novoTiro = {
				x = personagem.x - 60,
				y = personagem.y + 75,
				img = imgTiro
			}
			table.insert( tirosEsquerda, novoTiro )
		end
		atira = false
		tempoAtirar = delayTiro
	end
	
	-- Para cada tiro dado para ao lado DIREITO, tiro deve assumir a veocidade atribuída acima multiplicada por DT
	-- Caso o tiro chegue ao limite do frame da tela, ele é removido.
	for i, tiro in ipairs( tirosDireita ) do
		tiro.x = tiro.x  + ( velTiro * dt )
		-- COLISÃO AQUI
		if tiro.x > larguraTela then
			table.remove( tirosDireita, i )
		end
	end

	-- Para cada tiro dado para ao lado ESQUERDO, tiro deve assumir a veocidade atribuída acima multiplicada por DT
	-- Caso o tiro chegue ao limite do frame da tela, ele é removido.
	for i, tiro in ipairs( tirosEsquerda ) do
		tiro.x = tiro.x  - ( velTiro * dt )
		if tiro.x < 0 then
			table.remove( tirosEsquerda, i )
		end
	end
end
-- FIM: Função que determina a lógica para a execução dos tiros do PERSONAGEM PRINCIPAL

-- Função que determina a lógica para a execução dos tiros do PERSONAGEM BOSS (Mesma lógica aplicada acima)
function atirarChefao( dt )
	tempoAtirarChefao = tempoAtirarChefao - ( 1 * dt )
	if tempoAtirarChefao < 0 then
		atiraChefao = true
	end
	if love.keyboard.isDown( "k" ) and atiraChefao and direcaoChefao then
		novoTiro = {
			x = chefao.x + 65,
			y = chefao.y + 42,
			img = imgTiroChefao
		}
		if direcaoChefao then
			table.insert( tirosDireitaChefao, novoTiro )
		elseif not direcaoChefao then
			table.insert( tirosEsquerdaChefao, novoTiro )
		end
		atiraChefao = false
		tempoAtirarChefao = delayTiroChefao
	end

	if love.keyboard.isDown( "k" ) and atiraChefao then
		if direcaoChefao then
			novoTiro = {
				x = chefao.x + 65,
				y = chefao.y + 42,
				img = imgTiroChefao
			}
			table.insert( tirosDireitaChefao, novoTiro )
		elseif not direcaoChefao then
			novoTiro = {
				x = chefao.x - 65,
				y = chefao.y + 42,
				img = imgTiroChefao
			}
			table.insert( tirosEsquerdaChefao, novoTiro )
		end
	
		atiraChefao = false
		tempoAtirarChefao = delayTiroChefao
	end
	
	for i, tiro in ipairs( tirosDireitaChefao ) do
		tiro.x = tiro.x  + ( velTiroChefao * dt )
		if tiro.x > larguraTela then
			table.remove( tirosDireitaChefao, i )
		end
	end
	
	for i, tiro in ipairs( tirosEsquerdaChefao ) do
		tiro.x = tiro.x  - ( velTiroChefao * dt )
		if tiro.x < 0 then
			table.remove( tirosEsquerdaChefao, i )
		end
	end
end
-- FIM: Função que determina a lógica para a execução dos tiros do PERSONAGEM PRINCIPAL
