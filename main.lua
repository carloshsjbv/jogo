require 'fundo'
require 'personagem'

function love.load()

	-- Carregar tela inicial
	abreTela = false
	inicio = love.graphics.newImage( "Imagem/inicio.jpg" )
	imagemAtual = inicio
	timer = 0
 	-- FIM: Criar tela inicial

	-- Carregamento de personagens, vindos do arquivo personagens.lua
	criaPersonagemPrincipal( )
	criaChefao( )
	-- FIM: Carregamento de personagens, vindos do arquivo personagens.lua

end

function love.update(dt)
	movimentaPersonagemPrincipal( dt )
	atirar( dt )
	atirarChefao( dt )
	movimentaFundo( dt )
	colisoes()

	-- Timer que controla o tempo da imagem de tela inicial
	timer = timer + dt

	if timer > 1 then

	    if imagemAtual == inicio and estaVivo and love.keyboard.isDown( 'return' ) then
	      imagemAtual = nil
	    end
  	end
	-- TIMER: Timer que controla o tempo da imagem de tela inicial

end


function love.draw()

		if imagemAtual == inicio then
			love.graphics.draw(imagemAtual, 0, 0)
		end

		if imagemAtual == nil then
			-- Background
			love.graphics.draw( fundo, planoDeFundo.x, planoDeFundo.y )
			love.graphics.draw( fundoDois, planoDeFundo.x2, planoDeFundo.y )
			-- FIM: Background
			
	 		local fonte = love.graphics.newFont(16)
			love.graphics.print("VALOR X: " ..planoDeFundo.x, 10,60)
			love.graphics.print("VALOR X2: " ..planoDeFundo.x2, 10,80)
	
			local fonte = love.graphics.newFont(20)
			love.graphics.print("MR ROBOT:", 10,20)
			love.graphics.rectangle('line', 10, 60, 200, 15)
			love.graphics.rectangle('fill', 12, 60, 200, 15)


			
			-- Desenha personagem principal na tela
				if direcao then
					animationPersonagem:draw( imgPersonagem, personagem.x, personagem.y, 0, 1, 1, 30, 0 )
				elseif not direcao then
					animationPersonagem:draw( imgPersonagem, personagem.x, personagem.y, 0, -1, 1, 30, 0 )
				end
				-- FIM: Desenha personagem na tela

				-- Tiros personagem principal
				for i, tiro in ipairs( tirosDireita ) do
					love.graphics.draw( tiro.img, tiro.x, tiro.y, 0, 1, 1, imgTiro:getWidth(), imgTiro:getHeight() )
				end
				for i, tiro in ipairs( tirosEsquerda ) do
					love.graphics.draw( tiro.img, tiro.x, tiro.y, 0, -1, 1, imgTiro:getWidth(), imgTiro:getHeight() )
				end
			-- FIM: tiros


			-- BOSS: OS ELEMENTOS REFERENTES AO BOSS SÓ PODEM SER INICIADOS APÓS A CHEGADA AO FIM DA FASE.
			if planoDeFundo.x < 100 and chefaoVivo then
				-- Desenha boss na tela

				love.graphics.print("TRUMPPETE:", 500,20)
				love.graphics.rectangle('line', 500,60, vidaChefao*7, 15)
				love.graphics.rectangle('fill', 500,60, vidaChefao*7, 15)

				if direcaoChefao then
					animationChefao:draw( imgChefao, chefao.x, chefao.y, 0, 1, 1, 30, 0 )
				elseif not direcaoChefao then
					animationChefao:draw( imgChefao, chefao.x, chefao.y, 0, -1, 1, 30, 0 )
				end
				-- Desenha boss na tela

				-- Tiros do boss
				for i, tiro in ipairs( tirosDireitaChefao ) do
					love.graphics.draw( tiro.img, tiro.x, tiro.y, 0, 1, 1, imgTiro:getWidth(), imgTiro:getHeight() )
				end
				for i, tiro in ipairs( tirosEsquerdaChefao ) do
					love.graphics.draw( tiro.img, tiro.x, tiro.y, 0, -1, 1, imgTiro:getWidth(), imgTiro:getHeight() )
				end
				-- Tiros do boss
			end
			-- FIM: BOSS
		end
end

function colisoes()
		for j, tiro in ipairs( tirosDireita )  do
			if checaColisao(chefao.x, chefao.y, imgChefao:getWidth(), imgChefao:getHeight(), tiro.x, tiro.y, imgTiro:getWidth(), imgTiro:getHeight()) then
				
				table.remove(tiro, j)
				vidaChefao = vidaChefao - 1

				if vidaChefao == 0 then
					chefaoVivo = false;
				end

			end
		end
end

function checaColisao( tiroEmX, tiroEmY, larguraDoTiro, alturaDoTiro, objetoAtingidoEmX, objetoAtingidoEmY, larguraObjetoAtingido, alturaObjetoAtingido )
	return tiroEmX < objetoAtingidoEmX + larguraObjetoAtingido and objetoAtingidoEmX < tiroEmX + larguraDoTiro and tiroEmY < objetoAtingidoEmY + alturaObjetoAtingido and objetoAtingidoEmY < tiroEmY + alturaDoTiro
end
